#!/bin/bash

# Makes a booklet (A5 double page) out of all the pdfs in the current directory.

ls *.pdf|grep -v "\-book.pdf" > lsBooklet.txt
sed -i -e 's/$/"/' lsBooklet.txt
sed -i -e 's/^/pdfbook2 "/' lsBooklet.txt
echo `chmod +x lsBooklet.txt`
echo `./lsBooklet.txt`
rm lsBooklet.txt
