#!/bin/bash
#
# This is not my work. I hacked together multiple solutions.
#
# Found solution here: https://github.com/rclone/rclone/issues/249#issuecomment-371195391
#
# Make inotifywait ignore hidden files: https://unix.stackexchange.com/a/446667
# Make rclone ignore hidden files: https://forum.rclone.org/t/exclude-hidden-files/268/6

echo starting

while [[ true ]] ; do

    ping -c 1 google.com

    if [ $? -eq 0 ]; then

        rclone bisync --drive-skip-gdocs --drive-skip-shortcuts -v --exclude '.*{/**,}' /home/alex/aNotenSync NotenSyncGD:aNotenSync/

        retval=$?
        if [ $retval -ne 0 ]; then
            rclone bisync --resync --drive-skip-gdocs --drive-skip-shortcuts -v --exclude '.*{/**,}' /home/alex/aNotenSync NotenSyncGD:aNotenSync/
            retval=$?
        fi

        curl --retry 3 https://hc-ping.com/17de118d-7e38-4c4f-a892-36d40b2b5069/$retval
    fi

    inotifywait --recursive --timeout 300 -e modify,delete,create,move --exclude '/\.' /home/alex/aNotenSync

done

# Idee:
# unidirektionale Syncs: Wenn inotifywait eine Änderung feststellt, vom Laptop zum Repo unidirektional syncen.
# Wenn die Zeit abläuft: zuerst unidirektional vom Server auf den Laptop und danach unidirektional vom Laptop zum Server.
# jedes mal schauen, dass neuere Dateien nicht überschrieben werden.
