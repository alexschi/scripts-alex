#!/bin/bash

# This script deletes all files and subfolders in the ~/Temp-xxx directory that are older than 5 days.
# Add this or similar line to /etc/crontab: 43 *    * * *   root    /home/alex/bin/scripts-alex/delayedTrash.sh
# Soft version: Don't delete the files but move them to a folder called /home/alex/Trash/

mkdir -p /home/alex/Trash/

find /home/alex/Temp-2weeks/ -mindepth 1 -atime +14 -ctime +14 -mtime +14 -exec mv -t /home/alex/Trash/ {} \;
find /home/alex/Temp-1year/ -mindepth 1 -atime +400 -ctime +400 -mtime +400 -exec mv -t /home/alex/Trash/ {} \;
find /home/alex/Temp-10years/ -mindepth 1 -atime +4000 -ctime +4000 -mtime +4000 -exec mv -t /home/alex/Trash/ {} \;
find /home/alex/Pictures/Screenshots/ -mindepth 1 -atime +14 -ctime +14 -mtime +14 -exec mv -t /home/alex/Trash/ {} \;
find /home/alex/Downloads/ -mindepth 1 -atime +21 -ctime +21 -mtime +21 -exec mv -t /home/alex/Trash/ {} \;
find /home/alex/Trash/ -mindepth 1 -atime +14 -ctime +14 -mtime +14 -delete

# Remove empty directories after moving files
find /home/alex/Temp-2weeks/ -type d -empty -delete
find /home/alex/Temp-1year/ -type d -empty -delete
find /home/alex/Temp-10years/ -type d -empty -delete
find /home/alex/Pictures/Screenshots/ -type d -empty -delete
find /home/alex/Downloads/ -type d -empty -delete
find /home/alex/Trash/ -type d -empty -delete

exit 0
