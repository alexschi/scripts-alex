#!/bin/bash

# Define column width for each field
column_width_repo=45

# ANSI color codes
RED='\033[0;31m'
NC='\033[0m' # No Color

# Print table header with fixed-width columns
printf "%-${column_width_repo}s %-13s %-12s\n" \
       "Repository" "| Uncommitted" "| Up to Date"
printf "%-${column_width_repo}s %-13s %-12s\n" \
       "" "| Changes" "| w Remote"
echo "-------------------------------------------------------------"

# Function to check the status of a Git repository
check_repo_status() {
  local dir="$1"
  cd "$dir" || return

  # Initialize status variables for the main repo
  uncommitted_changes="| no"
  up_to_date="| -"
  color="$NC"

  # Check for uncommitted changes in the main repo (including submodule changes)
  if [ -n "$(git status -uall --porcelain)" ]; then
    uncommitted_changes="| yes"

    # Perform fetch and check if the local branch is up to date with the remote branch
    git fetch >/dev/null 2>&1
    LOCAL=$(git rev-parse @ 2>/dev/null)
    REMOTE=$(git rev-parse @{u} 2>/dev/null)
    if [ "$LOCAL" != "$REMOTE" ]; then
      up_to_date="| no"
    else
      up_to_date="| yes"
    fi
  fi

  # Set color to red if both uncommitted changes and not up to date
  if [[ "$uncommitted_changes" == "| yes" && "$up_to_date" == "| no" ]]; then
    color="$RED"
  fi

  # Print the result for this repository in aligned columns
  printf "${color}%-${column_width_repo}s %-13s %-12s${NC}\n" \
         "$dir" "$uncommitted_changes" "$up_to_date"

  # Return to the parent directory
  cd ..
}

# Loop through all subdirectories in the current directory
for dir in */; do
  if [ -d "$dir/.git" ]; then
    check_repo_status "$dir"
  fi
done
