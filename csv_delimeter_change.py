import pandas as pd
import sys

# Argumente für Eingabe- und Ausgabedatei
if len(sys.argv) != 4:
    print("Verwendung: python3 csv_delimiter_change.py <eingabedatei> <ausgabedatei> <eingabe_trennzeichen>")
    sys.exit(1)

eingabe_datei = sys.argv[1]
ausgabe_datei = sys.argv[2]
eingabe_trennzeichen = sys.argv[3]

# CSV-Datei einlesen und alle Spalten als String interpretieren
df = pd.read_csv(eingabe_datei, delimiter=eingabe_trennzeichen, dtype=str)

# CSV-Datei mit Komma als Trennzeichen speichern
df.to_csv(ausgabe_datei, index=False, sep=',')

print(f"Die Datei wurde erfolgreich als '{ausgabe_datei}' gespeichert.")
